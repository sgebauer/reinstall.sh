#!/bin/sh
set -eu

readonly DEBIANRELEASE="${DEBIANRELEASE:-buster}"
readonly DEBIANARCH="${DEBIANARCH:-amd64}"
readonly SHORT_HOSTNAME=$(echo "${HOSTNAME:-$(hostname -f)}" | cut -d. -f1)
readonly DOMAIN=$(echo "${HOSTNAME:-$(hostname -f)}" | cut -d. -f2-)
readonly NAMESERVER="${NAMESERVER:-9.9.9.9}"
# URI for the preseed file. Leave this empty for a manual installation.
readonly PRESEED_URI="${PRESEED_URI:-}"
# Use DHCP during installation? If not, reinstall will autodetect the current
# network configuration and pass it to the installer as static configuration.
readonly DHCP="${DHCP:-true}"
# Automatically reboot when we are done? Set to 'ask' to wait for a keypress.
readonly AUTO_REBOOT="${AUTO_REBOOT:-ask}"
# Base URI for the netinstall kernel and initrd
readonly NETBOOT_URI="${NETBOOT_URI:-https://deb.debian.org/debian/dists/${DEBIANRELEASE}/main/installer-${DEBIANARCH}/current/images/netboot/debian-installer/${DEBIANARCH}}"
# Path of the disk to install to, or 'auto' to autodetect from the current
# system, or '' to let the installer pick the first disk it finds.
readonly INSTALL_DISK="${INSTALL_DISK:-}"
# A URL for an SSH public key to use for remote installation (network-console)
readonly SSH_KEY_URL="${SSH_KEY_URL:-}"

# ========== You should not need to change anything below this line ==========

KERNEL_PARAMS="netcfg/choose_interface=auto netcfg/get_nameservers=${NAMESERVER} netcfg/get_hostname=${SHORT_HOSTNAME} netcfg/get_domain=${DOMAIN} netcfg/hostname=${SHORT_HOSTNAME} debian-installer/language=en debian-installer/country=DE debian-installer/locale=en_US.UTF-8 localechooser/preferred-locale=en_US.UTF-8 keyboard-configuration/xkb-keymap=us lowmem/low=note"

if [ -n "$PRESEED_URI" ]; then
  KERNEL_PARAMS="$KERNEL_PARAMS url=${PRESEED_URI}"
fi

# Generate network configuration
if [ "$DHCP" = true ]; then
  KERNEL_PARAMS="$KERNEL_PARAMS netcfg/disable_autoconfig=false"
else
  readonly GATEWAY=$(ip -4 route show default | awk '{print $3}')
  readonly IP=$(ip route get "$GATEWAY" | awk '{print $5}')
  readonly CIDR=$(ip -o addr show to "$IP" | awk '{print $4}' | cut -d/ -f2)
  readonly NETMASK=$(
    # CIDR%8 = number of 1s in first non-255 byte; CIDR/8 = number of 255 bytes
    echo "255.255.255.255.$(( (255 << (8 - (CIDR % 8))) & 255 )).0.0.0" | \
    cut -d. -f$(( 5 - (CIDR / 8) ))- | cut -d. -f-4
  )

  echo "Detected IP address ${IP}, netmask ${NETMASK} and gateway ${GATEWAY}"
  KERNEL_PARAMS="$KERNEL_PARAMS netcfg/disable_autoconfig=true netcfg/get_ipaddress=${IP} netcfg/get_netmask=${NETMASK} netcfg/get_gateway=${GATEWAY} netcfg/confirm_static=true"
fi

# Detect/set the disk for the root partition
if [ "$INSTALL_DISK" = 'auto' ]; then
  root_disk=$(mount | grep 'on / ' | cut -d ' ' -f 1)
  case "$root_disk" in
    /dev/sd??|/dev/vd??|/dev/xvd??)
      echo "Detected root disk ${root_disk%?}"
      KERNEL_PARAMS="$KERNEL_PARAMS partman-auto/disk=${root_disk%?}"
      ;;
    *)
      echo "Cound not detect root disk" >&2
      exit 1
      ;;
  esac
elif [ "$INSTALL_DISK" != "" ]; then
  KERNEL_PARAMS="$KERNEL_PARAMS partman-auto/disk=$INSTALL_DISK"
fi

# Configure network-console
if [ -n "$SSH_KEY_URL" ]; then
  if wget -nv -O /dev/null "$SSH_KEY_URL" >/dev/null; then
    KERNEL_PARAMS="$KERNEL_PARAMS anna/choose_modules=network-console network-console/authorized_keys_url=${SSH_KEY_URL}"
  else
    echo "Could not fetch SSH key from ${SSH_KEY_URL}"
    exit 1
  fi
fi

echo "Final kernel parameters: ${KERNEL_PARAMS}"

# Download netinstall kernel and initrd
mkdir -p /boot/reinstall
wget -nv -O /boot/reinstall/linux "${NETBOOT_URI}/linux"
wget -nv -O /boot/reinstall/initrd.gz "${NETBOOT_URI}/initrd.gz"

# Install grub config
readonly REINSTALL_PATH="/$(realpath --relative-to="$(stat -c '%m' /boot)" /boot/reinstall)"
cat >/etc/grub.d/09_reinstall <<EOF
#!/bin/sh
exec tail -n +3 \$0

menuentry "Reinstall: Debian ${DEBIANRELEASE} ${DEBIANARCH}" {
  linux ${REINSTALL_PATH}/linux $KERNEL_PARAMS
  initrd ${REINSTALL_PATH}/initrd.gz
}
EOF

chmod +x /etc/grub.d/09_reinstall
update-grub

case "$AUTO_REBOOT" in
  true) reboot ;;
  ask) echo "Done. Press enter to reboot."; read nothing && reboot ;;
  *) echo "Done. Reboot to start installation." ;;
esac
