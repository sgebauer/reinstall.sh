reinstall.sh
============

A fully automated Debian re-installer.

This script is loosely based on n-st's
[15_netinstall](https://github.com/n-st/15_netinstall).

Usage
-----

Check that you have ...
  * `wget` installed
  * grub2 (`/etc/grub.d` and the `update-grub` command should exist)
  * Enough free space in your `/boot` directory to fit the netinstall kernel and initramfs (about 30MB)

1. Download `reinstall.sh`
2. Change the first few lines to your needs
3. Run it!

You can also set environment variables instead of editing the script, e.g.
`DHCP=false ./reinstall.sh`.
